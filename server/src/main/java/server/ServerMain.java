package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerMain {
    public static void main(String[] args) {
        MessengerService server = new MessengerServiceImpl();
        try {
            MessengerService stub = (MessengerService) UnicastRemoteObject
                    .exportObject((MessengerService) server, 0);
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("MessengerService", stub);


        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
