package server;

import server.common.MedPlan;
import server.common.Medication;

import java.util.ArrayList;
import java.util.List;

public class MedPlanGenerator {
    private final long hour = 1000 * 60 * 60;

    private List<Medication> med (String name, int time1){

        List<Medication> list = new ArrayList<Medication>();
        list.add(new Medication(name, time1*hour));
        return list;
    }
    private List<Medication> med (String name, int time1, int time2){
        List<Medication> list = new ArrayList<Medication>();
        list.add(new Medication(name, time1*hour));
        list.add(new Medication(name, time2*hour));
        return list;
    }
    private List<Medication> med (String name, int time1, int time2, int time3){
        List<Medication> list = new ArrayList<Medication>();
        list.add(new Medication(name, time1*hour));
        list.add(new Medication(name, time2*hour));
        list.add(new Medication(name, time3*hour));
        return list;
    }

    private MedPlan medPlan1(int patientID){
        List<Medication> list = new ArrayList<Medication>();
        list.addAll(med("Ibuprofen 10", 2, 14, 20));
        list.addAll(med("Vitamin C", 8,  20));
        list.addAll(med("Ketonal", 9));

        return new MedPlan(patientID,list);
    }

    private MedPlan medPlan2(int patientID){
        List<Medication> list = new ArrayList<Medication>();
        list.addAll(med("Ibuprofen 20", 10, 14));
        list.addAll(med("Vitamin C", 8, 12,  20));
        list.addAll(med("Aspirin", 8, 14));
        list.addAll(med("Ketonal", 9, 20));

        return new MedPlan(patientID,list);
    }

    private MedPlan medPlan3(int patientID){
        List<Medication> list = new ArrayList<Medication>();
        list.addAll(med("Ibuprofen 30", 10, 12, 20));
        list.addAll(med("Vitamin C", 8, 12,  20));
        list.addAll(med("Aspirin", 8, 14));
        list.addAll(med("Ketonal", 9, 20));

        return new MedPlan(patientID,list);
    }

    public List<MedPlan> generateMedPlans(int patientID){
        List<MedPlan> medPlanList = new ArrayList<MedPlan>();
        medPlanList.add(medPlan1(patientID));
        medPlanList.add(medPlan2(patientID));
        medPlanList.add(medPlan3(patientID));

        return medPlanList;
    }
}
