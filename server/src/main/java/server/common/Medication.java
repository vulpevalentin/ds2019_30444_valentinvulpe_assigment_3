package server.common;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Medication implements Serializable {
    private String name;
    private long IntakeInterval;
    private Long IntakeTime; // list in millis since 00:00:00

    public Medication(String name, Long intakeInterval, Long intakeTime) {
        this.name = name;
        IntakeInterval = intakeInterval;
        IntakeTime = intakeTime;
    }

    public Medication(String name, Long intakeTime) {
        this.name = name;
        IntakeInterval = 1000 * 60 * 60 * 1; //1 hr before or after
        IntakeTime = intakeTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIntakeInterval() {
        return IntakeInterval;
    }

    public void setIntakeInterval(Long intakeInterval) {
        IntakeInterval = intakeInterval;
    }

    public Long getIntakeTime() {
        return IntakeTime;
    }

    public void setIntakeTime(Long intakeTime) {
        IntakeTime = intakeTime;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() == this.getClass()){
            if(
                    (((Medication) obj).getName() == null ? this.getName() == null : ((Medication) obj).getName().equals(this.getName()))
                    && (((Medication) obj).getIntakeTime() == null ? this.getIntakeTime() == null : ((Medication) obj).getIntakeTime().equals(this.getIntakeTime()))
                    && (((Medication) obj).getIntakeInterval() == null ? this.getIntakeInterval() == null : ((Medication) obj).getIntakeInterval().equals(this.getIntakeInterval()))
                    ){
                return true;
            }

        }
        return false;
    }
}

