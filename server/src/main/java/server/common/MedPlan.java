package server.common;

import java.io.Serializable;
import java.util.List;

public class MedPlan implements Serializable {
    private int patientID;
    private List<Medication> medicationList;

    public MedPlan(int patientID, List<Medication> medicationList) {
        this.patientID = patientID;
        this.medicationList = medicationList;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }
}
