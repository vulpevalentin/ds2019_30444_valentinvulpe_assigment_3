package server;

import server.common.MedPlan;
import server.common.Medication;

import java.rmi.RemoteException;
import java.util.Random;

public class MessengerServiceImpl implements MessengerService {


    @Override
    public String sendMessage(String clientMessage) {
        System.out.println("Client Connected");
        return "Client Connected".equals(clientMessage) ? "Server Connected" : null;
    }

    @Override
    public MedPlan sendMedplam(int num) throws RemoteException {
        MedPlanGenerator generator = new MedPlanGenerator();

        if(num> 2)
            num = 2;

        return generator.generateMedPlans(33).get(num);
    }

    @Override
    public void intakeSuccess(Medication medication) throws RemoteException {
        System.out.println("success received: " + medication.getName());

    }

    @Override
    public void intakeFailure(Medication medication) throws RemoteException {
        System.out.println("fail received: " + medication.getName());
    }




}