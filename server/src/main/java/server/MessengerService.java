package server;

import server.common.MedPlan;
import server.common.Medication;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MessengerService extends Remote {
    String sendMessage(String clientMessage) throws RemoteException;

    MedPlan sendMedplam(int num) throws RemoteException;

    void intakeSuccess(Medication medication) throws RemoteException;

    void intakeFailure(Medication medication) throws RemoteException;
}