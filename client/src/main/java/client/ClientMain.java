package client;

import gui.Gui;
import server.MessengerService;

import javax.swing.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientMain {
    public static void main(String[] args) {
        ConnectionManager connectionManager = new ConnectionManager();
        Gui gui = new Gui(new TimeKeeper(60 * 60, 3), connectionManager);
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setSize(500,500);
        //gui.pack();
//        JButton button = new JButton("Press");
//        gui.getContentPanel().add(button);
        gui.setVisible(true);
        //System.out.println(
        //connectionManager.getMedPlan().getMedicationList().get(0).getName());
        gui.runTime();



    }
}
