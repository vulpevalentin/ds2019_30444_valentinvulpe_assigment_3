package client;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeKeeper {

    private Date time;
    private long step;
    private Date endTime;
    private Date startDate;

    public TimeKeeper(long step, int periodInDays) {
        this.step = step;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date time = null;
        try {
            time = new Date(dateFormat.parse("2019-12-19 01:00:00").getTime());

            endTime = new Date(
                    time.getTime() + 1000*60*60*24 * periodInDays);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.time = time;

    }

    public Long getTimeOfDay(){
//        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
//        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
//        try {
//            return new Date(dateFormat.parse(dateFormat.format(time)).getTime()).getTime();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return null;

        return this.time.getTime() % (1000*60*60*24);
    }


    public boolean isPastEnd(){
        return time.after(endTime);
    }

    public void step(){
        time.setTime(time.getTime() + step);
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public long getStep() {
        return step;
    }

    public void setStep(long step) {
        this.step = step;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
