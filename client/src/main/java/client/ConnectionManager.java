package client;

import server.MessengerService;
import server.common.MedPlan;
import server.common.Medication;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ConnectionManager {
    private MessengerService server;


    public ConnectionManager() {
        try
        {
            Registry registry = LocateRegistry.getRegistry();
           // MessengerService server = null;
            server = (MessengerService) registry
                    .lookup("MessengerService");
            String responseMessage = server.sendMessage("Client Connected");
            System.out.println(responseMessage);

        } catch(
        RemoteException e)

        {
            e.printStackTrace();
        } catch(
        NotBoundException e)

        {
            e.printStackTrace();
        }
    }


    public MedPlan getMedPlan(int num){
        try {
            return server.sendMedplam(num);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void intakeSuccess(Medication medication){
        try {
            server.intakeSuccess(medication);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void intakeFailure(Medication medication){
        try {
            server.intakeFailure(medication);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
