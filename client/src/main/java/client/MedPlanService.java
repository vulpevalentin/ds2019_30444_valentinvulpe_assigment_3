package client;

import server.common.MedPlan;
import server.common.Medication;

import java.util.ArrayList;
import java.util.List;

public class MedPlanService {
    private MedPlan medPlan;
    //private List<Medication> currentList;
    private ConnectionManager connectionManager;

    public MedPlanService(){}

    public MedPlanService(MedPlan medPlan, ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        this.medPlan = medPlan;
        //this.currentList = new ArrayList<Medication>();
    }

//    public List<Medication> generateCurrentList(TimeKeeper timeKeeper){
//        currentList = new ArrayList<Medication>();
//        for(Medication medication : medPlan.getMedicationList()){
//            if(medication.getIntakeTime() - medication.getIntakeInterval() <= timeKeeper.getTimeOfDay()
//                    && medication.getIntakeTime() + medication.getIntakeInterval() >= timeKeeper.getTimeOfDay()){
//                currentList.add(medication);
//                medPlan.getMedicationList().remove(medication);
//            }
//        }
//        return currentList;
//    }

//    public List<Medication> updateCurrentList(TimeKeeper timeKeeper){
//        checkExpired(timeKeeper);
//        for(Medication medication : medPlan.getMedicationList()){
//            if(medication.getIntakeTime() - medication.getIntakeInterval() <= timeKeeper.getTimeOfDay()
//                    && medication.getIntakeTime() + medication.getIntakeInterval() >= timeKeeper.getTimeOfDay()){
//                currentList.add(medication);
//                medPlan.getMedicationList().remove(medication);
//            }
//        }
//        return currentList;
//    }




//    public void checkExpired (TimeKeeper timeKeeper){
//        if(medPlan != null){
//            for (Medication medication : currentList){
//                if(medication.getIntakeTime() + medication.getIntakeInterval() < timeKeeper.getTimeOfDay()){
//                    connectionManager.intakeFailure(medication);
//                    currentList.remove(medication);
//                }
//            }
//        }
//    }

    public void sendSuccess(Medication medication){
        connectionManager.intakeSuccess(medication);
        //currentList.remove(medication);
        System.out.println("success sent: " + medication.getName());
    }

    public boolean checkExpired(Medication medication, TimeKeeper timeKeeper){
        if(medication.getIntakeTime() + medication.getIntakeInterval() < timeKeeper.getTimeOfDay()){
            connectionManager.intakeFailure(medication);
            System.out.println("failure sent: " + medication.getName());
            return true;
        }
        else
            return false;
    }

    public boolean checkTakeable(Medication medication, TimeKeeper timeKeeper){
        if(medication.getIntakeTime() - medication.getIntakeInterval() < timeKeeper.getTimeOfDay())
            return true;
        else
            return false;
    }





    public MedPlan getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(MedPlan medPlan) {
        this.medPlan = medPlan;
    }

//    public List<Medication> getCurrentList() {
//        return currentList;
//    }
//
//    public void setCurrentList(List<Medication> currentList) {
//        this.currentList = currentList;
//    }
}
