package gui;

import client.ConnectionManager;
import client.MedPlanService;
import client.TimeKeeper;
import server.common.Medication;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import static java.lang.Thread.sleep;

public class Gui extends JFrame {
    private JLabel currenTime;
    private JPanel mainPanel;
    private JPanel contentPanel;
    private TimeKeeper timeKeeper;
    private ConnectionManager connectionManager;
    private MedPlanService medPlanService;
    private ArrayList<MedGui> medGuiList;
    private int day = 0;

    public Gui(TimeKeeper timeKeeper, ConnectionManager connectionManager){

        this.timeKeeper = timeKeeper;
        this.connectionManager = connectionManager;
        //medGuiList = new ArrayList<MedGui>();
        //this.medPlanService = new MedPlanService();
        add(mainPanel);


       // contentPanel.add(medGui.getMainPanel());




    }

    public void getMedPlan(int num){
        this.medPlanService = new MedPlanService(connectionManager.getMedPlan(num), connectionManager);
//        medPlanService.generateCurrentList(timeKeeper);
        System.out.println("New MedPlan!");
    }

    public void stepTime(){
        timeKeeper.step();
        updateTime();
    }
    public void updateTime(){
        SimpleDateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        currenTime.setText(dateFormat.format(this.timeKeeper.getTime()));
    }

    public void runTime(){
        //getMedPlan();
        //updateCurrentList();
        while (!timeKeeper.isPastEnd()){
//            if(timeKeeper.getTimeOfDay() == 0)
//                getMedPlan();
            //updateCurrentList();
            stepTime();

            if(timeKeeper.getTime().getTime() % (1000 * 60 * 60 * 24 )== 0){
                day++;
                createMedPlanGUIs();
                System.out.println("A new day!");

            }

            for(MedGui medGui : medGuiList){

                if(medGui.isActive()) {


                    medGui.updateTimeLeft(timeKeeper);

                    if (medPlanService.checkExpired(medGui.getMedication(), timeKeeper)) {
                        medGui.getMainPanel().setVisible(false);
                        medGui.setActive(false);
                        //medGuiList.remove(medGui);
                    }

                    if(medPlanService.checkTakeable(medGui.getMedication(), timeKeeper)){
                        medGui.enableButton();
                    }
                }

            }





            try {
                sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
          }
    }


//    private void updateCurrentList(){
//        List<Medication> currentList = medPlanService.updateCurrentList(timeKeeper);
//        for(Medication medication : currentList){
//            MedGui medGui = new MedGui(medication,timeKeeper, medPlanService);
//            contentPanel.add(medGui.getMainPanel());
//            //mainPanel.repaint();
//        }
//
//
//    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
        contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel,BoxLayout.PAGE_AXIS));

        createMedPlanGUIs();



    }

    private void createMedPlanGUIs(){
        medGuiList = new ArrayList<MedGui>();
        getMedPlan(day);
        for(Medication medication: medPlanService.getMedPlan().getMedicationList()){
            MedGui medGui = new MedGui(medication,timeKeeper, medPlanService);
            //
            medGuiList.add(medGui);
            contentPanel.add(medGui.getMainPanel());
        }
    }

    public JPanel getContentPanel() {
        return contentPanel;
    }

    public void setContentPanel(JPanel contentPanel) {
        this.contentPanel = contentPanel;
    }
}
