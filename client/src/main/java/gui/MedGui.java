package gui;

import client.MedPlanService;
import client.TimeKeeper;
import server.common.Medication;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class MedGui {
    private JPanel mainPanel;
    private JButton takenButton;
    private JLabel timeLeft;
    private JLabel medName;
    private JLabel timeToTakeText;
    private Medication medication;
    private MedPlanService medPlanService;
    private boolean active = true;

    public MedGui() {

        timeLeft.setText("time left");
        medName.setText("name");

    }

    public MedGui(final Medication medication, TimeKeeper timeKeeper, final MedPlanService medPlanService) {
        this.medPlanService = medPlanService;
        this.medication = medication;
        updateTimeLeft(timeKeeper);
        setTimeToTake();
        //timeLeft.setText("time left");
        medName.setText(medication.getName());


        //takenButton.disable();
        takenButton.setEnabled(false);
        takenButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                medPlanService.sendSuccess(medication);
                mainPanel.setVisible(false);
                active = false;


            }
        });
    }

    public void enableButton(){
        takenButton.setEnabled(true);
    }

    public void updateTimeLeft(TimeKeeper timeKeeper) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        long timeLeftlong = medication.getIntakeTime() + medication.getIntakeInterval() - timeKeeper.getTimeOfDay();
        timeLeft.setText(format.format(new Date(timeLeftlong)));
    }

    public void setTimeToTake(){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        long timeToTake = medication.getIntakeTime();
        timeToTakeText.setText(format.format(new Date(timeToTake)));
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }


    public Medication getMedication(){
        return medication;
    }

    public JLabel getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(JLabel timeLeft) {
        this.timeLeft = timeLeft;
    }

    public JLabel getMedName() {
        return medName;
    }

    public void setMedName(JLabel medName) {
        this.medName = medName;
    }

    public JButton getTakenButton() {
        return takenButton;
    }

    public void setTakenButton(JButton takenButton) {
        this.takenButton = takenButton;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
